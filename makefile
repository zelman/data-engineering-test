PROCESS_TEMPLATE=processing.cf.yaml
CONFIG_FILE=dev.cfg
CONFIG=dev
PROCESS_STACK_NAME=process-${CONFIG}


validate-templates:
	$(eval TEMPLATES = $(shell find . -name '*.cf.yaml' | sort | uniq))
	@for template in $(TEMPLATES); do \
		echo "--- :cloudformation: Validating $$template" && \
		aws cloudformation validate-template \
			--template-body file://./$$template \
			--output table; \
		if [ $$? -ne 0 ]; then \
			echo "^^^ Validation failed, oh no!!" && \
			exit 1; \
		fi \
	done
	@echo "Validation Successful, good job!!"

unit-tests:
	@echo "run pytest across the code base"


deploy-process-stack:
	@if [ -f ./cloudformation/$(PROCESS_TEMPLATE) ] ; then \
		echo "--- :cloudformation: Deploying stack $(PROCESS_STACK_NAME)"; \
		aws cloudformation deploy \
			--no-fail-on-empty-changeset \
			--capabilities CAPABILITY_NAMED_IAM \
			--template ./cloudformation/$(PROCESS_TEMPLATE) \
			--stack-name $(PROCESS_STACK_NAME) \
			--parameter-overrides \
				Configuration=$(CONFIG) \
				$(shell cat ./config/$(CONFIG_FILE)); \
	else \
		echo --- :sadpanda: No Process template found.; \
	fi;

deploy-lambda-code:
	@echo "---run the deploy.sh for each lambda to upload the code"
	cd ./src/lambda/file_validation && ./deploy.sh nmi_consumption_validation
	cd ./src/lambda/convert_values  && ./deploy.sh nmi_consumption_transformation



delete-process-stack:
	@echo "--- :cloudformation: Deleting process stack"; \
	aws cloudformation delete-stack --stack-name $(PROCESS_STACK_NAME)

deploy: test deploy-process-stack deploy-lambda-code
destroy: delete-extract-stack
test: validate-templates unit-tests
