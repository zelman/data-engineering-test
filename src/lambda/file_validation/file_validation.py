import json
import urllib.parse
from io import StringIO

import boto3
import pandas as pd



s3 = boto3.client('s3')

def lambda_handler(event, context):
    main(event)
    
def main(event):
    for msg in event['Records']:
        records = json.loads(msg['body'])
        for rec in records['Records']:
            # Get the object from the event
            bucket = rec['s3']['bucket']['name']
            key = urllib.parse.unquote_plus(rec['s3']['object']['key'], encoding='utf-8')
            response = s3.get_object(Bucket=bucket, Key=key)
            filename = key.split("/")[-1]
            print("File: {}".format(filename))

            # Load the dataframe
            reads = pd.read_csv(response['Body'])


            # Validate datetime format
            print("Validating datetime format")
            validate_AESTTime = reads['AESTTime'].apply(pd.to_datetime, errors='coerce')
            bad_date = reads[validate_AESTTime.isna()].copy()
            bad_date['Reason'] = 'Invalid AESTTime'

            # Validate no duplicate times - if it fails then all good.
            print("Validating no duplicate times")
            try: 
                dupes = pd.concat(g for _,g in reads.groupby('AESTTime') if len(g) > 1)
            except:
                dupes = pd.DataFrame()
            dupes['Reason'] = 'Duplicate times'

            # Validate resolutions match expected
            # need to load in the info file, match against filename to get the resolution, then use a shift or something
            # similar to get the time difference between each row, and compare against expected resolution

            # Validate Unit
            print("Validating Unit")
            units = ['mwh','kwh','wh']
            bad_unit = reads[~reads['Unit'].str.lower().isin(units)].copy()
            bad_unit['Reason']='Invalid Unit'
            
            # Validate Quantity is numeric
            print("Validating Quantity")
            validate_Quantity = pd.to_numeric(reads.Quantity,errors='coerce')
            bad_quantity = reads.loc[validate_Quantity.isna()].copy()
            bad_quantity['Reason']='Invalid Quantity'
            
            
            # Write failures to error file
            print("Confirming validity")
            bad_df = bad_quantity.append(bad_unit).append(dupes).append(bad_date)
            if not bad_df.empty:
                print("Errors in file")
                output_key = 'error/'+filename
                bad_buffer = StringIO()
                bad_df.to_csv(bad_buffer)      
                s3.put_object(Bucket=bucket, Body=bad_buffer.getvalue(), Key=output_key)
                # for the purposes of this exercise, let all records through.  In prod, may want to exclude bad records
            
            # put back to S3
            print("Writing to validated")
            output_buffer = StringIO()
            reads.to_csv(output_buffer,index=False)
            
            filename = key.split("/")[-1]

            output_key = 'validated/'+filename
            s3.put_object(Bucket=bucket, Body=output_buffer.getvalue(), Key=output_key)

    return

if __name__ == "__main__":
    main(event)
