import boto3
import pandas as pd
import botocore
import sys
import datetime as dt


s3 = boto3.client('s3')


def lambda_handler(event, context):
    main(event)
    
def main(event):
    service_id = event['queryStringParameters']['service_id']
    rule = event['queryStringParameters']['rule']
    return process(service_id, rule)

def process(service_id, rule):
    bucket = "nmi-consumption"
    key = "transformed/{}.csv".format(service_id)
    mapping_key = "nmi_info.csv"
    try:
        response = s3.get_object(Bucket=bucket, Key=key)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "NoSuchKey":
            # return api response 404
            print("The file for {} does not exist".format(service_id))
            exit(1)

    reads = pd.read_csv(response['Body'])

    response = s3.get_object(Bucket=bucket, Key=mapping_key)
    mappings = pd.read_csv(response['Body'])

    # Get date and time components
    reads['Date']=pd.to_datetime(reads['local'],format='%Y-%m-%d %H:%M:%S').dt.date
    reads['Time']=pd.to_datetime(reads['local'],format='%Y-%m-%d %H:%M:%S').dt.time

    # Get the average read for each interval for the year
    gbt = reads.groupby('Time')['whQuantity'].mean()
    gbt_df = gbt.to_frame()



    # rule 1: first cross over the average, last cross under the average
    if rule == "cross-average":
        gbt_df['prevWhQuantity'] = gbt_df['whQuantity'].shift()
        # get the average for all times
        m = gbt.mean()

        opening = gbt_df.loc[(gbt_df['whQuantity'] > m) &( gbt_df['prevWhQuantity'] < m)].iloc[0]
        opening_time = opening.name

        closing = gbt_df.loc[(gbt_df['whQuantity'] < m) &( gbt_df['prevWhQuantity'] > m)].iloc[-1]
        closing_time = closing.name

    elif rule == "largest-difference":
        # rule 2: largest increase in consumption, largest decrease in consumption
        gbt_df['change'] = gbt_df.diff()

        opening_time = gbt_df.iloc[gbt_df['change'].argmax()].name
        closing_time = gbt_df.iloc[gbt_df['change'].argmin()].name

    else:
        # return api response 400
        print("{} is not a known rule".format(rule))
        exit(1)


    # return api 200 response
    print("Opening time: {}".format(opening_time))
    print("Closing time: {}".format(closing_time))
    return 

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Wrong number of args. Usage python3 opening_hours.py <Service_Id> <rule>")
        exit(1)
    service_id = sys.argv[1]
    rule = sys.argv[2].lower()
    known_rules = ['largest-difference','cross-average']
    if not rule in known_rules:
        print("Rule {} not accepted. Allowed rules are: {}".format(rule, known_rules))
    process(service_id, rule)
