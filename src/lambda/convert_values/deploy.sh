#!/bin/sh
 
set -e
 
if [ -z "$1" ]
then
  echo "ERROR: Provide a function name as a parameter"
  exit 1
fi
 
mkdir zip_folder
cp *.py  zip_folder
cp ../../external/numpy-1.20.2-cp37-cp37m-manylinux1_x86_64.whl zip_folder
cp ../../external/pandas-1.2.4-cp37-cp37m-manylinux1_x86_64.whl zip_folder 
cd zip_folder

unzip numpy-1.20.2-cp37-cp37m-manylinux1_x86_64.whl
unzip pandas-1.2.4-cp37-cp37m-manylinux1_x86_64.whl

pip3 install --target . pytz

rm -r *.whl *.dist-info 

zip -r9 function.zip .
 
aws lambda update-function-code --function-name $1 --zip-file fileb://function.zip --publish
 
cd ..
 
rm -r zip_folder
