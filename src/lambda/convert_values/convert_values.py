import json
import urllib.parse
from io import StringIO

import boto3
import pandas as pd
import datetime as dt


s3 = boto3.client('s3')

def conversion(row):
    if row.Unit.lower() == 'mwh':
        return row.Quantity * 1000000
    if row.Unit.lower() == 'kwh':
        return row.Quantity * 1000
    return row.Quantity



def lambda_handler(event, context):
    main(event)
    
def main(event):
    for msg in event['Records']:
        records = json.loads(msg['body'])
        for rec in records['Records']:
            # Get the object from the event
            bucket = rec['s3']['bucket']['name']
            key = urllib.parse.unquote_plus(rec['s3']['object']['key'], encoding='utf-8')
            response = s3.get_object(Bucket=bucket, Key=key)
            filename = key.split("/")[-1]
            service_id = filename.split(".")[0]
            print("File: {}".format(filename))
            
            # Load the dataframes
            reads = pd.read_csv(response['Body'])
            
            mapping_key = "nmi_info.csv"
            response = s3.get_object(Bucket=bucket, Key=mapping_key)
            mappings = pd.read_csv(response['Body'])

            # append the converted quantity
            print("Convert to Wh")
            reads['whQuantity'] = reads.apply(lambda x: conversion(x), axis=1)
            
            # append local time
            print('Converting to local time')
            if mappings.loc[mappings['Nmi'] == service_id,'State'].empty:
                reads['local'] = pd.to_datetime(reads['AESTTime'])
            elif mappings.loc[mappings['Nmi'] ==service_id,'State'].item() == "WA":
                reads['local'] = pd.to_datetime(reads['AESTTime'])+ dt.timedelta(hours=3)
            elif mappings.loc[mappings['Nmi'] ==service_id,'State'].item() == "VIC" or mappings.loc[mappings['Nmi'] ==service_id,'State'].item() == "NSW":
                reads['local'] = pd.to_datetime(reads['AESTTime'])
                reads.loc[reads['local'].between('2017-10-01 02:00:00', '2018-04-01 01:59:59'),'local'] = reads['local'] + dt.timedelta(hours=1)
            else:
                reads['local'] = pd.to_datetime(reads['AESTTime'])
                
            # put back to S3
            print("Writing to transformed")
            output_buffer = StringIO()
            reads.to_csv(output_buffer,index=False)
            
            filename = key.split("/")[-1]

            output_key = 'transformed/'+filename
            s3.put_object(Bucket=bucket, Body=output_buffer.getvalue(), Key=output_key)

    return

if __name__ == "__main__":
    main(event)
