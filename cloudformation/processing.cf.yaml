AWSTemplateFormatVersion: "2010-09-09"
Description: Infra stack
Parameters:
  LandingBucketName:
    Type: String
    Description: Name of S3 Bucket where the external files will land
  TransformationFunctionName:
    Type: String
    Description: Name of the Lambda that the API will hit
  TransformationHandler:
    Type: String
    Description: The name of function the Lambda shall call [package].[function]
  TransformationDescription:
    Type: String
    Description: Description of the Lambda function
  ValidationFunctionName:
    Type: String
    Description: Name of the Lambda that the API will hit
  ValidationHandler:
    Type: String
    Description: The name of function the Lambda shall call [package].[function]
  ValidationDescription:
    Type: String
    Description: Description of the Lambda function
  # APIGatewayName:
  #   Type: String
  #   Description: Name of the API Gateway
Resources:
  ValidationLambda:
    Type: "AWS::Lambda::Function"
    DependsOn: IAMLambdaExecutionRole
    Properties:
      Code:
        ZipFile: " "
      Description: !Ref ValidationDescription
      FunctionName: !Ref ValidationFunctionName
      Handler: !Ref ValidationHandler
      MemorySize: 256
      Role: !GetAtt IAMLambdaExecutionRole.Arn
      Runtime: python3.7
      Timeout: 120
      TracingConfig:
        Mode: PassThrough
      Tags:
        - Key: Name
          Value: !Ref ValidationFunctionName
  IAMLambdaExecutionRole:
    DependsOn: 
      - ValidationQueue
      - TransformationQueue
      - LandingS3Bucket
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Principal:
              Service: lambda.amazonaws.com
            Action:
              - "sts:AssumeRole"
      Description: "IAM Role for the Lambda"
      Policies:
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Effect: "Allow"
                Action: 
                  - "s3:PutObject"
                  - "s3:GetObject"
                Resource:
                  - !Join
                    - "/"
                    - - !GetAtt LandingS3Bucket.Arn
                      - "*"
              - Effect: "Allow"
                Action: "s3:ListBucket"
                Resource: 
                  - !GetAtt LandingS3Bucket.Arn
              - Effect: Allow
                Action:
                - sqs:ReceiveMessage
                - sqs:DeleteMessage
                - sqs:GetQueueAttributes
                - sqs:ChangeMessageVisibility
                Resource: 
                  - !GetAtt ValidationQueue.Arn
                  - !GetAtt ValidationDLQ.Arn
                  - !GetAtt TransformationQueue.Arn
                  - !GetAtt TransformationDLQ.Arn
              - Effect: Allow
                Action:
                - logs:CreateLogGroup
                - logs:CreateLogStream
                - logs:PutLogEvents
                Resource: "*"
          PolicyName: !Sub "Lambda-execution-policy"
      Path: "/"
      RoleName: !Sub "Lambda-execution-role"
  ValidationEventSource:
    DependsOn: ValidationLambda
    Type: "AWS::Lambda::EventSourceMapping"
    Properties:
      Enabled: true
      EventSourceArn: !GetAtt ValidationQueue.Arn
      FunctionName: !Ref ValidationLambda
  ValidationQueue:
    Type: "AWS::SQS::Queue"
    Properties:
      QueueName: !Ref ValidationFunctionName
      RedrivePolicy:
        deadLetterTargetArn: !GetAtt ValidationDLQ.Arn 
        maxReceiveCount: 2
      VisibilityTimeout: 120
  ValidationDLQ:
    Type: "AWS::SQS::Queue"
    Properties:
      QueueName: !Sub "${ValidationFunctionName}-dlq"
  SQSQueuePolicy:
    Type: AWS::SQS::QueuePolicy
    Properties:
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal: "*"
            Action: SQS:SendMessage
            Resource: "*"
            Condition:
              ArnLike:
                aws:SourceArn:  !Join
                  - ""
                  - - "arn:aws:s3:::"
                    - !Ref LandingBucketName
      Queues:
        - Ref: ValidationQueue
        - Ref: TransformationQueue
  LandingS3Bucket:
    DependsOn: 
      - ValidationQueue
      - TransformationQueue
      - SQSQueuePolicy
    Type: "AWS::S3::Bucket"
    Properties:
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: aws:kms
      BucketName: !Ref LandingBucketName
      NotificationConfiguration:
        QueueConfigurations:
          - Event: "s3:ObjectCreated:Put"
            Filter:
              S3Key:
                Rules:
                  - Name: "suffix"
                    Value: ".csv"
                  - Name: "prefix"
                    Value: "ConsumptionData"
            Queue: !GetAtt ValidationQueue.Arn
          - Event: "s3:ObjectCreated:Put"
            Filter:
              S3Key:
                Rules:
                  - Name: "suffix"
                    Value: ".csv"
                  - Name: "prefix"
                    Value: "validated"
            Queue: !GetAtt TransformationQueue.Arn
      PublicAccessBlockConfiguration:
          BlockPublicAcls: true
          BlockPublicPolicy: true
          IgnorePublicAcls: true
          RestrictPublicBuckets: true
  TransformationLambda:
    Type: "AWS::Lambda::Function"
    DependsOn: IAMLambdaExecutionRole
    Properties:
      Code:
        ZipFile: " "
      Description: !Ref TransformationDescription
      FunctionName: !Ref TransformationFunctionName
      Handler: !Ref TransformationHandler
      MemorySize: 256
      Role: !GetAtt IAMLambdaExecutionRole.Arn
      Runtime: python3.7
      Timeout: 120
      TracingConfig:
        Mode: PassThrough
  TransformationEventSource:
    DependsOn: TransformationLambda
    Type: "AWS::Lambda::EventSourceMapping"
    Properties:
      Enabled: true
      EventSourceArn: !GetAtt TransformationQueue.Arn
      FunctionName: !Ref TransformationLambda
  TransformationQueue:
    Type: "AWS::SQS::Queue"
    Properties:
      QueueName: !Ref TransformationFunctionName
      RedrivePolicy:
        deadLetterTargetArn: !GetAtt TransformationDLQ.Arn 
        maxReceiveCount: 2
      VisibilityTimeout: 120
  TransformationDLQ:
    Type: "AWS::SQS::Queue"
    Properties:
      QueueName: !Sub "${TransformationFunctionName}-dlq"
  