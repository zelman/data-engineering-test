# data-engineering-test

# Details
See Data Engineering Test_20201012.pptx

# Prerequisites
- Python 3
- Pytest
- Boto3
- pandas
- AWS Cli configured to your AWS account

# Running the makefile
- **make deploy**: run all the tests, then deploy the solution
- **make destroy**: remove the cloudformation stacks
- **make test**: validate the cloudformation templates, and run pytest
